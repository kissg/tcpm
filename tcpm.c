/* Trivial CP/M emulator. 1.1 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <ctype.h>
#include "simz80.h"

BYTE ram[65536];

WORD af[2];
int af_sel;
struct ddregs regs[2];
int regs_sel;
WORD ir;
WORD ix;
WORD iy;
WORD sp;
WORD pc;
WORD IFF;

#define	AF af[af_sel]
#define	BC regs[regs_sel].bc
#define	DE regs[regs_sel].de
#define	HL regs[regs_sel].hl

#define	JUMP		0xc3
#define	RET		0xc9
#define	JMP(address)	JUMP,lreg(address),hreg(address)
#define	PATCH(code)	0x76,code,0x00
#define	CTRLZ		0x1a

#define	BIOSENTRY	0x0000
#define	IOBYTE		0x0003
#define DEFDRIVE	0x0004
#define	BDOSENTRY	0x0005
#define	FCB		0x005c
#define	FCB2		0x006c
#define	DEFDMA		0x0080
#define	TPA		0x0100
#define	BIOSBASE	0xff00

BYTE lowmem[] = {
	JMP(BIOSBASE+3),/* BIOS */
	0,		/* iobyte */
	0,		/* default drive = A: */
	JMP(BIOSBASE-4)	/* BDOS */
};

BYTE *empty_fcb = "\0           ";

BYTE highmem[] = {
	0,0,		/* fefa top of stack */
	PATCH(255),RET,	/* fefc BDOS hook */
	JMP(0xff34),	/* ff00 BIOS COLD BOOT */
	JMP(0xff38),	/* ff03 BIOS WARM BOOT */
	JMP(0xff3c),	/* ff06 BIOS console status */
	JMP(0xff40),	/* ff09 BIOS console input */
	JMP(0xff44),	/* ff0c BIOS console output */
	JMP(0xff48),	/* ff0f BIOS list output */
	JMP(0xff4c),	/* ff12 BIOS punch output */
	JMP(0xff50),	/* ff15 BIOS reader input */
	JMP(0xff54),	/* ff18 BIOS home disk */
	JMP(0xff58),	/* ff1b BIOS select disk */
	JMP(0xff5c),	/* ff1e BIOS set track */
	JMP(0xff60),	/* ff21 BIOS set sector */
	JMP(0xff64),	/* ff24 BIOS set DMA address */
	JMP(0xff68),	/* ff27 BIOS read sector */
	JMP(0xff6c),	/* ff2a BIOS write sector */
	JMP(0xff70),	/* ff2d BIOS list status */
	JMP(0xff74),	/* ff30 BIOS SECTRAN (unimplemented) */
	0,		/* dummy */
/*-----------------------------------------------------------*/
	PATCH(0),RET,	/* ff34 BIOS COLD BOOT */
	PATCH(1),RET,	/* ff38 BIOS WARM BOOT */
	PATCH(2),RET,	/* ff3c BIOS console status */
	PATCH(3),RET,	/* ff40 BIOS console input */
	PATCH(4),RET,	/* ff44 BIOS console output */
	PATCH(5),RET,	/* ff48 BIOS list output */
	PATCH(6),RET,	/* ff4c BIOS punch output */
	PATCH(7),RET,	/* ff50 BIOS reader input */
	PATCH(8),RET,	/* ff54 BIOS home disk */
	PATCH(9),RET,	/* ff58 BIOS select disk */
	PATCH(11),RET,	/* ff5c BIOS set track */
	PATCH(12),RET,	/* ff60 BIOS set sector */
	PATCH(13),RET,	/* ff64 BIOS set DMA */
	PATCH(14),RET,	/* ff68 BIOS read sector */
	PATCH(15),RET,	/* ff6c BIOS write sector */
	PATCH(16),RET,	/* ff70 BIOS list status */
	PATCH(17),RET,	/* ff74 BIOS SECTRAN (unimplemented) */
};

char *bdos_name[] = {
/* 0*/	"system reset","console input","console output","reader_input",
/* 4*/	"punch output","list output","console i/o","get i/o byte",
/* 8*/	"set i/o byte","print string","read con buf","console status",
/*12*/	"get version","reset disk","select disk","open file",
/*16*/	"close file","search first","search next","delete  file",
/*20*/	"read seq.","write seq.","create file","rename file",
/*24*/	"get login vec","get current disk","set DMA","get alloc addr",
/*28*/	"write prot.","get r/o vec","set file attr","get DPB addr",
/*32*/	"user code","read random","write random","file size",
/*36*/	"set random","reset drive","unknown","unknown",
/*40*/	"write zero","unknown","unknown","unknown"
};

void init_mem(void)
{
	memcpy((void*)ram,             (void*)lowmem, sizeof(lowmem));
	memcpy((void*)(ram+BIOSBASE-6),(void*)highmem,sizeof(highmem));
	memcpy((void*)(ram+FCB),       (void*)empty_fcb,12);
	memcpy((void*)(ram+FCB2),      (void*)empty_fcb,12);
}

struct inbuf {
	BYTE maxlen;
	BYTE len;
	BYTE line[0];
};

struct _fcb {
/*0*/	BYTE drive;		/* 0=def 1=A...15=P */
/*1*/	BYTE filename[8];
/*9*/	BYTE filetype[3];
/*12*/	BYTE extent;		/* 0..31 (not used) */
/*13*/	BYTE s1,s2;		/* (not used) */
/*15*/	BYTE record_count;	/* 0..128 (not used) */
/*16*/	FILE *fp;
	BYTE text;
	BYTE lastchr;
	BYTE d[16-sizeof(FILE*)-2*sizeof(BYTE)];
/*32*/	BYTE current_record;	/* (not used) */
/*33*/	BYTE r[3];		/* (not used) */
};

char filename[128];

int traceflag=0,debug=0;
FILE *configfile=NULL;
int console_in=0, console_out=1, list, reader, punch;
long block,nonblock;
WORD dma=DEFDMA;
int usercode=0;

struct drvmap {
	char path[128];
	int readonly;
	int uppercase;
	int text;
} drivemap0 = {".",1,0,0},
*drivemap[16] = {&drivemap0};

void dumpblock(BYTE * dma)
{
	int i,j;

	fputc('\n',stderr);
	for (i=0; i<8; i++) {
		for (j=0; j<16; j++)
			fprintf(stderr,"%.2x ",dma[16*i+j]);
		for (j=0; j<16; j++)
			fputc(isprint(dma[16*i+j])?dma[16*i+j]:'.',stderr);
		fputc('\n',stderr);
	}
}

BYTE conin(int mode)
{
	static int waitmode=0;
	static int buffer=-1;
	int cnt;
	BYTE retval;

if (traceflag) fprintf(stderr,"conin(%d),buffer=%x\n",mode,buffer);
	switch (mode) {
	case 0:		/* nowait */
		if (buffer!=-1) {
			retval = buffer & 0xff;
			buffer = -1;
			return retval;
		}
		if (!waitmode) {
			if (fcntl(console_in,F_SETFL,nonblock)==-1)
				perror("cpm: fcntl(nonblock) error");
			waitmode=1;
		}
		cnt=read(console_in,&retval,1);
		if (cnt==0) return 0;	/* EOF */
		if (cnt==-1) {
			if (errno==EAGAIN) return 0;
			perror("Error on console input");
			exit(2);
		}
		return retval;
	case 1:		/* wait */
		if (buffer!=-1) {
			retval = buffer & 0xff;
			buffer = -1;
			return retval;
		}
		if (waitmode) {
			if (fcntl(console_in,F_SETFL,block)==-1)
				perror("cpm: fcntl(block) error");
			waitmode=0;
		}
		cnt=read(console_in,&retval,1);
		if (cnt==0) {
			fprintf(stderr,
				"Error on console input: End of file\n");
			exit(2);
		}
		if (cnt==-1) {
			perror("Error on console input");
			exit(2);
		}
		return retval;
	case 2:		/* status */
		if (buffer!=-1) return 0xff;
		if (!waitmode) {
			if (fcntl(console_in,F_SETFL,nonblock)==-1)
				perror("cpm: fcntl(nonblock) error");
			waitmode=1;
		}
		cnt=read(console_in,&retval,1);
if (traceflag){
 int i;
 fprintf(stderr,"mode=%d cnt=%d...\n",mode,cnt);
 for (i=0; i<cnt; i++) fprintf(stderr,"%.2x ",(&retval)[i]);
}
		if (cnt==0) {
			buffer=-1;
			return 0;
		}
		else if (cnt==-1) {
			if (errno==EAGAIN) {
				buffer=-1;
				return 0;
			}
			perror("Error on console input");
			exit(2);
		}
		else if (cnt!=1) {
			fprintf(stderr,"cpm: invalid byte count: %d\n",cnt);
			exit(2);
		}
		buffer=retval;
if (traceflag) fprintf(stderr,"buffer=%x\n",buffer);
		return 0xff;
	}
	/* NOTREACHED */
	return 0xff;
}

struct drvmap *process_fcb(struct _fcb *fcb)
{
	struct drvmap *drv;
	char c,*p;
	int i;

	/* get drive */
	if (debug) fprintf(stderr,"map %c:\"%11s\" to ",
			'A'+(fcb->drive?fcb->drive-1:ram[DEFDRIVE]),
			fcb->filename);

	drv=drivemap[fcb->drive ? fcb->drive-1 : ram[DEFDRIVE] ];
	if (drv==NULL) {
		if (debug) fputc('\n',stderr);
		fprintf(stderr,"cpm: unmapped drive %d\n",(int)(fcb->drive));
		return NULL;
	}

	/* get filename */
	strcpy(filename,drv->path);
	for (p=filename; *p; p++);
	if (p[-1]!='/') *p++='/';
	for (i=0; i<8 && !isspace(c=fcb->filename[i]); i++) {
		if (drv->uppercase) *p++=c;
		else *p++=tolower(c);
	}
	if (!isspace(toascii(fcb->filetype[0]))) *p++='.';
	for (i=0; i<3 && !isspace(c=toascii(fcb->filetype[i])); i++) {
		if (drv->uppercase) *p++=c;
		else *p++=tolower(c);
	}
	*p='\0';
	if (debug) fprintf(stderr,"\"%s\" %s,%s\n",
		filename,
		drv->readonly?"ro":"rw",
		drv->text?"text":"binary");
	return drv;
}

int open_file(int mode)
{
	struct _fcb *fcb=(struct _fcb*)(ram+DE);
	struct drvmap *drv;
	FILE *f;

	Sethreg(AF,0xff);
	if ( !(drv=process_fcb(fcb)) ) return -1;
	if (traceflag) fprintf(stderr,"[filename=%s]",filename);

	if (mode==0) {  /* existing file */
		if (drv->readonly) f=fopen(filename,"r");
		else {
			f=fopen(filename,"r+");
			if (f==NULL && errno==EACCES) f=fopen(filename,"r");
		}
	}
	else {		/* new file */
		if (drv->readonly) f=NULL;
		else f=fopen(filename,"w+");
	}

	if (f==NULL) {
		fprintf(stderr,"cpm: cannot %s file %s",
			mode?"create":"open", filename);
		perror(" ");
		return Sethreg(AF,0xff);
	}

	fcb->fp=f;	/* Solaris alatt ebbe hal bele :-( */
	fcb->text=drv->text;
	return Sethreg(AF,0);
}

void trace(char *msg)
{
	if (!traceflag) return;
	fprintf(stderr,"%s\n",msg);
}

void bios_boot(void)
{
	trace("BIOS cold boot");
	exit(0);
}

void bios_wboot(void)
{
	trace("BIOS warm boot");
	exit(0);
}

void bios_const(void)
{
	trace("BIOS console status");
	Sethreg(AF,conin(2));
}

void bios_conin(void)
{
	trace("BIOS console input");
	Sethreg(AF,conin(1));
}

void bios_conout(void)
{
	BYTE c;

	trace("BIOS console output");
	c=lreg(BC);
	write(console_out,&c,1);
}

void bios_list(void)
{
	trace("BIOS list output");
}

void bios_punch(void)
{
	trace("BIOS punch output");
}

void bios_reader(void)
{
	trace("BIOS console input");
}

void bios_setdma(void)
{
	trace("BIOS set dma");
}

void bios_listst(void)
{
	trace("BIOS list status");
}

void bdos(void)
{
	char *p,*wptr;
	char filename2[128];
	struct inbuf *conbuf;
	BYTE c;
	int i,j;
	struct drvmap *map;
	struct _fcb *fcb;

	if (traceflag)
		fprintf(stderr,"\n[BDOS function %d=%s]",
			lreg(BC),bdos_name[lreg(BC)]);

	switch (lreg(BC)) {
	default:
		fprintf(stderr,
			"Unimplemented BDOS function %d at address %.4X\n",
			lreg(BC), GetWORD(sp));
		exit(1);
	case 0:		/* warm boot */
		traceflag=0;
		bios_wboot();
		/* NOTREACHED */
	case 1:		/* read console */
		Sethreg(AF,conin(1));
if (traceflag) fprintf(stderr,"[A=%.2x]",hreg(AF));
		return;
	case 2:		/* write console */
		c=lreg(DE);
if (traceflag) fprintf(stderr,"[e=%.2x]",c);
		write(console_out,&c,1);
		return;
	case 3:		/* reader input */
		if (read(reader,&c,1)==0) c=CTRLZ;
		Sethreg(BC,c);
		return;
	case 4: 	/* punch output */
		c=lreg(DE);
		write(punch,&c,1);
		return;
	case 5:		/* list output */
		c=lreg(DE);
		write(list,&c,1);
		return;
	case 6:		/* direct console I/O */
		c=lreg(DE);
		if (c != 0xff) write(console_out,&c,1);
		else Sethreg(AF,conin(0));
		return;
	case 7:		/* get iobyte */
		Sethreg(AF,ram[IOBYTE]);
		return;
	case 8:		/* set iobyte */
		ram[IOBYTE]=lreg(DE);
		return;
	case 9:		/* print string */
		for (p=ram+DE; *p!='$'; p++)
			write(console_out,p,1);
		return;
	case 10: 	/* read console buffer */
		conbuf=(struct inbuf*)(ram+DE);
		wptr=conbuf->line;
		conbuf->len=0;
		while (1) switch(c=conin(1)) {
		case '\003':
			if (conbuf->len==0) exit(3);
			/* FALL-THROUGH */
		default:
if (traceflag) fprintf(stderr,"[c=%.2x]",c);
			if (conbuf->maxlen==conbuf->len) return;
			*wptr++=c;
			conbuf->len++;
			break;
		case '\r':
		case '\n':
			if (traceflag) fprintf(stderr,"[%d:%*s]",
				(int)(conbuf->len),(int)(conbuf->len),
				conbuf->line);
			return;
		}
		/* NOTREACHED */
	case 11:	/* check console status */
		Sethreg(AF,conin(2));
		if (traceflag) fprintf(stderr,"[status=%d]",hreg(AF));
		return;
	case 12: 	/* return version number */
		Sethreg(AF,0x22);
		Sethreg(BC,0);
		HL=0x0022;
		return;
	case 13:	/* reset disk system */
		dma=DEFDMA;
		return;
	case 14:	/* set default drive */
		if (lreg(DE)<16) ram[DEFDRIVE]=lreg(DE);
		return;
	case 15:	/* open file */
		open_file(0);
		if (traceflag) fprintf(stderr,"[status=%d]",hreg(AF));
		return;
	case 16:	/* close file */
		fcb=(struct _fcb*)(ram+DE);
		Sethreg(AF, fclose(fcb->fp) ? 0xff : 0);
		return;
	case 19:	/* delete file */
		Sethreg(AF,0xff);
		map=process_fcb((struct _fcb*)(ram+DE));
		if (!map || map->readonly) return;
		Sethreg(AF, unlink(filename) ? 0xff : 0);
		return;
	case 20:	/* read sequential */
		Sethreg(AF,0xff);
		fcb=(struct _fcb*)(ram+DE);
		if (traceflag) fprintf(stderr,"[file=%d,pos=%lx]",
				fileno(fcb->fp),ftell(fcb->fp));
		for (i=0,p=ram+dma; i<128; i++) {
			j=getc(fcb->fp);
			if (j==EOF) {
/*
if (i==126)
fprintf(stderr,"file=%d pos=%lx",fileno(fcb->fp),ftell(fcb->fp));
fprintf(stderr,"i=%d j=%x ",i,j);
*/
				if (i) j=CTRLZ; else return;
}
			if (j=='\n' && fcb->lastchr!='\r' && fcb->text) {
				j='\r';
				ungetc('\n',fcb->fp);
			}
			fcb->lastchr = *p++ = j&0xff;
		}
		Sethreg(AF,0);
		if (debug>2 && traceflag) dumpblock(ram+dma);
		return;
	case 21:	/* write sequential */
		fcb=(struct _fcb*)(ram+DE);
		if (traceflag) fprintf(stderr,"[file=%d,pos=%lx]",
				fileno(fcb->fp),ftell(fcb->fp));
		for (i=0,p=ram+dma; i<128; i++,p++)
			if (*p!='\r' || !fcb->text)
				if (putc(*p,fcb->fp)==EOF) break;
		Sethreg(AF,i!=128);
		if (debug>2 && traceflag) dumpblock(ram+dma);
		return;
	case 22:	/* create file */
		open_file(1);
		if (traceflag) fprintf(stderr,"[status=%d]",hreg(AF));
		return;
	case 23:	/* rename file */
		Sethreg(AF,0xff);
		map=process_fcb((struct _fcb*)(ram+DE));
		if (!map || map->readonly) return;
		strcpy(filename2,filename);
		map=process_fcb((struct _fcb*)(ram+DE+16));
		if (!map || map->readonly) return;
		Sethreg(AF, rename(filename2,filename) ? 0xff : 0);
		return;
	case 24:	/* return login vector */
		j=0;
		for (i=15; i>=0; i++) j = 2*j + (drivemap[i]!=NULL);
		Sethreg(AF, lreg(j));
		Sethreg(BC, hreg(j));
		HL=j;
		return;
	case 25:	/* return current disk */
		Sethreg(AF, ram[DEFDRIVE]);
		return;
	case 26:	/* set DMA */
		dma=DE;
		return;
	case 28:	/* write protect default drive */
		drivemap[ram[DEFDRIVE]]->readonly=1;
		return;
	case 29:	/* get R/O vector */
		j=0;
		for (i=15; i>=0; i++)
			j = 2*j + (drivemap[i] && drivemap[i]->readonly);
		Sethreg(AF, lreg(j));
		Sethreg(BC, hreg(j));
		HL=j;
		return;
	case 30:	/* set file attributes */
		Sethreg(AF,0);
		return;
	case 32:	/* get/set user code */
		if (lreg(DE)==0xff) Sethreg(AF,usercode);
		else usercode=lreg(DE);
		return;
	}
}

void usage(void)
{
	fprintf(stderr,"Usage: cpm [-c configfile] program [args...]\n");
}

void prepare_fcb(struct _fcb *fcb, char *arg)
{
	char *s=arg, *d;
	int i;

	for (i=0; i<11; i++) fcb->filename[i]=' ';

	if (isascii(s[0]) && s[1] && s[1]==':') {
		fcb->drive=toupper(s[0])-'A'+1;
		s+=2;
	}
	else fcb->drive=0;

	d=fcb->filename;
	i=8;
	while (i-- && *s && *s!='.') *d++=toupper(*s++);
		
	while (*s && *s!='.') s++;
	d=fcb->filetype;
	i=3;
	if (*s++=='.') while (i-- && *s && *s!='.') *d++=toupper(*s++);

	if (debug)
		fprintf(stderr,"FCB: \"%s\" -> \"%11s\"\n",arg,fcb->filename);
}

int in(port)
unsigned int port;
{
	return 0;
}

void out(port,value)
unsigned int port;
unsigned char value;
{
	return;
}

char *strlower(char *s)
{
	register char *p=s;
	if (!p) return s;
	while (*p) *p=tolower(*p),p++;
	return s;
}

void configerr(char *l)
{
	fprintf(stderr, "cpm: error in config file: \"%s\"",l);
}

#define STREQ(t,s)	(strcmp((t),(s))==0)

int read_config(FILE *cf)
{
	char line[256];
	char *p,*t;

	while (fgets(line,sizeof(line),cf)) {
		if (line[0]=='#') continue;	/* comment */
		p=line;
		t=strlower(strtok(p," \t\n"));
		if (!t) continue;		/* blank line */
		else if (STREQ(t,"dev")) {
			int *devptr, flags;
			char *d,*f;

			d=strlower(strtok(NULL," \t\n"));
			if (!d) { configerr(line); return 0; }
			else if (STREQ(d,"console_in")) {
				flags = O_RDONLY;
				devptr = &console_in;
			}
			else if (STREQ(d,"console_out")) {
				flags = O_CREAT;
				devptr = &console_out;
			}
			else if (STREQ(d,"list")) {
				flags = O_CREAT;
				devptr = &list;
			}
			else if (STREQ(d,"reader")) {
				flags = O_RDONLY;
				devptr = &reader;
			}
			else if (STREQ(d,"punch")) {
				flags = O_CREAT;
				devptr = &punch;
			}
			else { configerr(line); return 0; }

			f=strtok(NULL," \t\n");
			if (!f) { configerr(line); return 0; }

			t=strlower(strtok(NULL," \t\n"));
			if (!t) { }
			else if (STREQ(t,"append")) {
				if (flags==O_CREAT) flags = O_WRONLY|O_APPEND;
			}
			else { configerr(line); return 0; }

			close(*devptr);
			if ((*devptr=open(f,flags,0644))==-1) {
				fprintf(stderr,
					"cpm: cannot open device file %s",f);
				perror(" ");
				return 0;
			}
			if (debug) fprintf(stderr,
				"Opening file %s as \"%s\" device\n",f,d);
			continue;
		}
		else if (STREQ(t,"map")) {
			char *dir;
			int drive, readonly=0, uppercase=0, text=0;
			struct stat st;

			t=strlower(strtok(NULL," \t\n"));
			if (t && 'a'<=t[0] && t[0]<='p' && t[1]==':' && !t[2])
				drive=t[0]-'a';
			else { configerr(line); return 0; }

			dir=strtok(NULL," \t\n");
			if (!dir) { configerr(line); return 0; }
			if (stat(dir,&st)==-1) {
				fprintf(stderr,
					"cpm: cannot map directory %s",dir);
				perror(" ");
				return 0;
			}
			if (!S_ISDIR(st.st_mode)) {
				fprintf(stderr,
					"cpm: %s is not a directory",dir);
				return 0;
			}
			while ((t=strlower(strtok(NULL," \t\n")))!='\0') {
				if (STREQ(t,"ro")) readonly=1;
				else if (STREQ(t,"text")) text=1;
				else if (STREQ(t,"uppercase")) uppercase=1;
				else { configerr(line); return 0; }
			}
			if (drivemap[drive] && drivemap[drive]!=&drivemap0)
				free(drivemap[drive]);
			if ((drivemap[drive]=malloc(sizeof(**drivemap)))==NULL) {
				fprintf(stderr,"cpm: not enough memory\n");
				return 0;
			}
			strncpy(drivemap[drive]->path,dir,128);
			drivemap[drive]->readonly=readonly;
			drivemap[drive]->uppercase=uppercase;
			drivemap[drive]->text=text;
			if (debug) fprintf(stderr,
				"Mapping drive %c: to directory \"%s\","
				" attributes: %s,%s,%s\n",
				drive+'A',dir,
				readonly?"ro":"rw",
				text?"text":"binary",
				uppercase?"uppercase":"lowercase");
		}
		else { configerr(line); return 0; }

		t=strtok(NULL," \t\n");
	}
	return -1;
}

int main(int argc, char **argv)
{
	extern char *optarg;
	extern int optind;
	struct stat st;
	int c, fd, i;
	char *d, *s;
	FASTWORK addr;

	/* parse args */
	while ((c = getopt(argc, argv, "tdc:")) != EOF)
		switch(c) {
		case 'c':	/* config file name */
			configfile=fopen(optarg,"r");
			break;
		case 'd':
			debug++;
			break;
		case 't':
			traceflag++;
			break;
		default:
			usage();
			exit(3);
		}

	init_mem();

	/* read configuration */
	list=open("/dev/null",O_WRONLY);
	punch=open("/dev/null",O_WRONLY);
	reader=open("/dev/null",O_RDONLY);
	if (!configfile && (s=getenv("TCPMRC"))!=NULL) configfile=fopen(s,"r");
	if (!configfile) {
		s=getenv("HOME");
		if (s) {
			sprintf(filename,"%s/.tcpmrc",s);
			configfile=fopen(filename,"r");
		}
	}
	if (configfile) if (!read_config(configfile)) exit(2);

	/* load program */
	if (optind>=argc) {
		usage();
		exit(3);
	}
	if ((fd=open(argv[optind++],O_RDONLY))==-1) {
		--optind;
		fprintf(stderr,"cpm: cannot open program file %s",argv[optind]);
		perror(" ");
		exit(2);
	}
	fstat(fd,&st);
	if (st.st_size > BIOSBASE-4-TPA) {
		fprintf(stderr,"cpm: program %s is too large\n",argv[--optind]);
		exit(2);
	}
	if (read(fd,ram+TPA,st.st_size)==-1) {
		fprintf(stderr,"cpm: cannot load program %s",argv[--optind]);
		perror("");
		exit(2);
	}

	/* prepare command line */
	for (d=ram+DEFDMA+1,i=optind; i<argc; i++) {
		s=argv[i];
		*d++=' ';
		while (*s) *d++=toupper(*s++);
	}
	ram[DEFDMA] = (int)d-(int)(ram+DEFDMA+1);
	if (debug) fprintf(stderr,
		"cmdline: %d,\"%*s\"\n",
		(int)ram[DEFDMA],(int)ram[DEFDMA],ram+DEFDMA+1);

	/* prepare default FCB */
	if (optind<argc) prepare_fcb((struct _fcb*)(ram+FCB),argv[optind++]);
	if (optind<argc) prepare_fcb((struct _fcb*)(ram+FCB2),argv[optind++]);


	addr=0x100;
	sp=BIOSBASE-6;
	/* start execution */
	while (1) {
		addr=simz80(addr);
		switch (ram[pc]) {
		case 255:bdos();	break;
		case 0:	 bios_boot();	break;
		case 1:	 bios_wboot();	break;
		case 2:	 bios_const();	break;
		case 3:	 bios_conin();	break;
		case 4:	 bios_conout(); break;
		case 5:	 bios_list();	break;
		case 6:	 bios_punch();	break;
		case 7:	 bios_reader();	break;
		case 13: bios_setdma();	break;
		case 16: bios_listst();	break;
		default:
			fprintf(stderr,"Unimplemented hook at address %.4X\n",
				addr-2);
			exit(1);
		}
		addr++;
	}
}
