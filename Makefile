BASEDIR		= /usr/local
BINDIR		= $(BASEDIR)/bin
MANDIR		= $(BASEDIR)/man/man1

OPTIMIZE      = -O2
#OPTIMIZE       = -g

CPMOBJS	      =	tcpm.o simz80.o

INSTALL	      = install

CFLAGS        =	-Wall -Wstrict-prototypes $(OPTIMIZE) $(OPTIONS)

all:		tcpm

tcpm:		$(CPMOBJS)
		$(CC) $(CFLAGS) $(CPMOBJS) -o $@

simz80.c:	simz80.pl
		perl -w simz80.pl >simz80.c

install:	all
		$(INSTALL) -s -c -m 755 tcpm $(BINDIR)

clean:
		rm -f *.o *~ core tcpm simz80.c

tcpm.o:		tcpm.c simz80.h
simz80.o:	simz80.c simz80.h
